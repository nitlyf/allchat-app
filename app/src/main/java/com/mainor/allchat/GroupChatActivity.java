package com.mainor.allchat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SimpleTimeZone;


public class GroupChatActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private ImageButton sendMesageButton;
    private ScrollView mScrowview;
    private TextView  ShowMessage;
    private EditText InputTextMesage;
    private String CurrentGroupName,currentUserID,currentUserName ,currentDate ,currentTime;
    private FirebaseAuth mAuth;
    private DatabaseReference UserRef ,GroupRef,GroupMessageKeyRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);


        CurrentGroupName= getIntent().getExtras().get("GroupName").toString();
        Toast.makeText(GroupChatActivity.this, CurrentGroupName, Toast.LENGTH_SHORT).show();


         mAuth = FirebaseAuth.getInstance();
         currentUserID = mAuth.getCurrentUser().getUid();
         UserRef= FirebaseDatabase.getInstance().getReference().child("Users");
        GroupRef= FirebaseDatabase.getInstance().getReference().child("Groups").child(CurrentGroupName);

        InitializeFileds();
           GetUserInfo();
             sendMesageButton.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v)
                 {
                    SendMessageInfoToDtatabase();
                    InputTextMesage.setText("");
                 }
             });




    }

    @Override
    protected void onStart() {
        super.onStart();

        GroupRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s)
            {
                if(dataSnapshot.exists())
                {
                    DisaplayMassage(dataSnapshot);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s)
            {
                if(dataSnapshot.exists())
                {
                    DisaplayMassage(dataSnapshot);
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    private void InitializeFileds()
    {
        mToolbar=(Toolbar)findViewById(R.id.Group_chat_layout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Welcome to "+CurrentGroupName);


        sendMesageButton=(ImageButton) findViewById(R.id.send_message_button);
        mScrowview=(ScrollView) findViewById(R.id.my_Scroll_view);
        ShowMessage=(TextView) findViewById(R.id.group_chat_text_display);
        InputTextMesage=(EditText) findViewById(R.id.input_group_message);



    }
    private void GetUserInfo()
    {
        UserRef.child(currentUserID);
        UserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
               if (dataSnapshot.exists())
               {
                   currentUserName = dataSnapshot.child("name").getValue().toString();
               }
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }




    private void SendMessageInfoToDtatabase()

    {
        String messageKey= GroupRef.push().getKey();
        String message= InputTextMesage.getText().toString();
        if (TextUtils.isEmpty(message))
        {
            Toast.makeText(GroupChatActivity.this, "pleasse write a message", Toast.LENGTH_SHORT).show();
        }
        else
            {
                Calendar calForDte=Calendar.getInstance();
                SimpleDateFormat currentDateFormat= new SimpleDateFormat("MMM dd yyyy");
                currentDate=currentDateFormat.format(calForDte.getTime());

                Calendar calForTime=Calendar.getInstance();
                SimpleDateFormat currentTimeFormat= new SimpleDateFormat("hh:mm:a");
                currentTime=currentTimeFormat.format(calForTime.getTime());

                HashMap<String, Object> GroupMessageKey=new HashMap<>();
                GroupRef.updateChildren(GroupMessageKey);

                GroupMessageKeyRef=GroupRef.child(messageKey);
                    HashMap<String, Object> MessageInfoMap=new HashMap<>();
                    MessageInfoMap.put("name",currentUserName);
                    MessageInfoMap.put("date",currentDate);
                    MessageInfoMap.put("message",message);
                    MessageInfoMap.put("time",currentTime);
                GroupMessageKeyRef.updateChildren(MessageInfoMap);
            }
    }



    private void DisaplayMassage(DataSnapshot dataSnapshot)
    {
        Iterator iterator= dataSnapshot.getChildren().iterator();
        while (iterator.hasNext())
        {
            String chatDate=(String)((DataSnapshot)iterator.next()).getValue();
            String chatMessage=(String)((DataSnapshot)iterator.next()).getValue();
            String chatTime=(String)((DataSnapshot)iterator.next()).getValue();
            ShowMessage.append(chatMessage+"\n "+chatDate + "    " + chatTime+ "\n\n\n");
        }
    }

}





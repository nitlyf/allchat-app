package com.mainor.allchat;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TabsAccessorAdaptor extends FragmentPagerAdapter {
    public TabsAccessorAdaptor(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ChatFragment chatFragment= new ChatFragment();
                return chatFragment;

            case 1:
                ContactsFragment contactsFragment= new ContactsFragment();
                return contactsFragment;

            case 2:
                GroupFragment groupFragment= new GroupFragment();
                return groupFragment;

        default:
            return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:

                return "chat";

            case 1:
                ContactsFragment contactsFragment= new ContactsFragment();
                return "contacts";

            case 2:
                GroupFragment groupFragment= new GroupFragment();
                return "groups";

            default:
                return null;
        }
    }
    }


package com.mainor.allchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    private FirebaseUser user;
  //  private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private Button LoginButton;
    private Button FacebookLoginButton;
    private EditText UserEmail;
    private EditText UserPassword;
    private TextView NeedNewAccountLink;
    private TextView ForgetPasswordLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth= FirebaseAuth.getInstance();
       // currentUser=mAuth.getCurrentUser();

        IntializeFields();

        NeedNewAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AskUserToRegister();


            }
        });

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               AllowUserToLoin() ;
            }
              });
            }

    private void AllowUserToLoin()
    {
        String email = UserEmail.getText().toString();
        String password = UserPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "please enter your email", Toast.LENGTH_SHORT).show();
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "please enter your password", Toast.LENGTH_SHORT).show();
        }
        else
            {
              mAuth.signInWithEmailAndPassword(email,password)
                      .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                          @Override
                          public void onComplete(@NonNull Task<AuthResult> task)
                          {
                          if (task.isSuccessful())
                          {
                             SendUserToMainActivity();
                              Toast.makeText(LoginActivity.this, "You are logged in", Toast.LENGTH_SHORT).show();
                          }

                          }
                      });
            }
    }


    private void IntializeFields() {
        LoginButton =(Button)findViewById(R.id.login);
        FacebookLoginButton=(Button)findViewById(R.id.facebook_login);
        UserEmail =(EditText)findViewById(R.id.login_email);
        UserPassword =(EditText)findViewById(R.id.login_password);
        NeedNewAccountLink=(TextView)findViewById(R.id.need_new_account_link);
        ForgetPasswordLink=(TextView)findViewById(R.id.login_forget_password_link);
    }


//    @Override
//    protected void onStart() {
//
//        if(user!=null){
//            AskUserToLogIn();
//        }
//        super.onStart();
//    }

//    private void AskUserToLogIn()
//        {
//    Intent LoginIntent =new Intent(LoginActivity.this,MainActivity.class);
//        startActivity(LoginIntent);
//    }


    private void SendUserToMainActivity(){
        Intent MainIntent =new Intent(LoginActivity.this,MainActivity.class);
        MainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(MainIntent);
        finish();
    }
private void AskUserToRegister() {

        Intent RegisterIntent =new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(RegisterIntent);
        }
        }





package com.mainor.allchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {
    private Toolbar  toolbar;
    private ViewPager Pager;
    private TabLayout mtablayout;
    private TabsAccessorAdaptor mTabsAccessorAdaptor;
    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;
    private RecyclerView recyclerView;
    private DatabaseReference rootRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);

         mAuth= FirebaseAuth.getInstance();
         currentUser=mAuth.getCurrentUser();
         rootRef= FirebaseDatabase.getInstance().getReference();

         toolbar= (Toolbar) findViewById(R.id.main_page_bar);
         setSupportActionBar(toolbar);
         getSupportActionBar().setTitle("Babe");


         Pager= (ViewPager)findViewById(R.id.main_tabs_pager);
         mTabsAccessorAdaptor= new TabsAccessorAdaptor(getSupportFragmentManager());
         Pager.setAdapter(mTabsAccessorAdaptor);

         mtablayout = (TabLayout) findViewById(R.id.main_tabs);
         mtablayout.setupWithViewPager(Pager);

       // recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
       // recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if(currentUser==null)
        {
            AskUserToLogIn();

        }
        else
        {
            VerifyUserExitence();
        }
    }

    private void VerifyUserExitence()
    {

      String currentUserID=mAuth.getCurrentUser().getUid();
       rootRef.child("users").child(currentUserID).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(DataSnapshot dataSnapshot)
           {
             if ((dataSnapshot.child("name").exists()))

             {
                 Toast.makeText(MainActivity.this, "Wellcome", Toast.LENGTH_SHORT).show();
             }else
                 {
                     SendUserToSettingsActivity();
                 }



           }

           @Override
           public void onCancelled( DatabaseError databaseError) {

           }
       });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
         super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected( MenuItem item)
    {
         super.onOptionsItemSelected(item);
        if(item.getItemId()==R.id.main_logout_button)
        {
            mAuth.signOut();
            AskUserToLogIn();
        }
        if(item.getItemId()==R.id.main_settings_button)
        {
         SendUserToSettingsActivity();
        }
        if(item.getItemId()==R.id.main_find_friends_button)
        {
        }

        if(item.getItemId()==R.id.Create_group_button)
        {
            RequestNewGroup();
        }
        return true;
        }

    private void RequestNewGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialog);
        builder.setTitle("Enter Group Name");
        final EditText GroupNameField = new EditText(MainActivity.this);
        GroupNameField.setHint("e.g The Lovers");
        builder.setView(GroupNameField);
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String groupName = GroupNameField.getText().toString();
                if (TextUtils.isEmpty(groupName)) {
                    Toast.makeText(MainActivity.this, "please write group name", Toast.LENGTH_SHORT).show();
                } else {
                    creatNewGroup(groupName);
                }
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

        private void creatNewGroup(final String groupName)
        {
            rootRef.child("Groups").child(groupName).setValue("")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                         if (task.isSuccessful())
                         {
                             Toast.makeText(MainActivity.this, groupName+" Created...", Toast.LENGTH_SHORT).show();
                         }
                        }

        });
        }



    private void AskUserToLogIn() {

        Intent LoginActivity =new Intent(MainActivity.this, com.mainor.allchat.LoginActivity.class);
        LoginActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(LoginActivity);
        finish();
    }


    private void SendUserToSettingsActivity() {

        Intent SettingsIntent =new Intent(MainActivity.this, com.mainor.allchat.SettingsActivity.class);
        SettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(SettingsIntent);
        finish();

    }

}

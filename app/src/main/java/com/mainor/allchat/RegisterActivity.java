package com.mainor.allchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private Button CreateAccountButton;
    private EditText UserEmail;
    private EditText UserPassword;
    private TextView AlreadyHasAnAccount;
    private FirebaseAuth mAuth;
    private DatabaseReference Rootref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mAuth= FirebaseAuth.getInstance();
        Rootref=FirebaseDatabase.getInstance().getReference();


        IntializeFields();


        AlreadyHasAnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AskUserToLogIn();

            }
        });
           CreateAccountButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   CreateNewAccount();
               }
           });
    }

    private void CreateNewAccount() {

        String email = UserEmail.getText().toString();
        String password = UserPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "please enter your email", Toast.LENGTH_SHORT).show();
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "please enter your password", Toast.LENGTH_SHORT).show();
        }
        else
            {

            }

              mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                      if(task.isSuccessful());

                        {
                            String CurrentUserId=mAuth.getCurrentUser().getUid();
                            Rootref.child("User").child(CurrentUserId).setValue("");


                            SendUserToMainActivity();
                            Toast.makeText(RegisterActivity.this, "Account created succesfully", Toast.LENGTH_SHORT).show();
                        }

                    }

                       //create error masage
                });
    }



    private void IntializeFields() {
       CreateAccountButton =(Button)findViewById(R.id.register);

        UserEmail =(EditText)findViewById(R.id.register_email);
        UserPassword =(EditText)findViewById(R.id.register_password);
        AlreadyHasAnAccount=(TextView)findViewById(R.id.login_already_have_an_account_link);


    }

    private void AskUserToLogIn()
    {
        Intent LoginIntent =new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(LoginIntent);
    }



    private void SendUserToMainActivity(){
        Intent MainIntent =new Intent(RegisterActivity.this,MainActivity.class);
        MainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(MainIntent);
        finish();
    }
    }




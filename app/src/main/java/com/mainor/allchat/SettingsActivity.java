package com.mainor.allchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    private Button UpdateSettingsAccount;
    private EditText Username;
    private EditText userStatus;
    private CircleImageView circleImageView;
    private String CurentUserID;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Initialize();


        mAuth = FirebaseAuth.getInstance();
        CurentUserID = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();

       UpdateSettingsAccount.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               updateSettings();
           }
       });

    }


    private void Initialize()
    {
        UpdateSettingsAccount= (Button) findViewById(R.id.update_profile_id);
        Username = (EditText) findViewById(R.id.set_profile_id);
        userStatus = (EditText) findViewById(R.id.status_profile_id);
        circleImageView = (CircleImageView) findViewById(R.id.set_profile_image);
    }


    private void updateSettings() {
        String setUsername = Username.getText().toString();
        String setStatus = userStatus.getText().toString();
        if
        (TextUtils.isEmpty(setUsername))
        {
            Toast.makeText(SettingsActivity.this, "please set username", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(setStatus)) {
            Toast.makeText(SettingsActivity.this, "Account created succesfully", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> ProfileMap = new HashMap<>();
            ProfileMap.put("uid", CurentUserID);
            ProfileMap.put("name", setUsername);
            ProfileMap.put("status", setStatus);
            rootRef.child("users").child(CurentUserID).setValue(ProfileMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(Task<Void> task)
                        {
                            if (task.isSuccessful())
                            {
                                SendUserToMainActivity();
                                Toast.makeText(SettingsActivity.this, "profile updated", Toast.LENGTH_SHORT).show();
                            } else
                                {
                                String message = task.getException().toString();
                                Toast.makeText(SettingsActivity.this, "error" + message, Toast.LENGTH_SHORT).show();
                                }
                        }


                    });

        }

    }

    private void SendUserToMainActivity(){
        Intent MainIntent =new Intent(SettingsActivity.this,MainActivity.class);
        MainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(MainIntent);
        finish();
    }


    }





